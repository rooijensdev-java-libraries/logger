# Logger
Project repository found on [Git](https://gitlab.com/rooijensdev-java-libraries/logger).

This logger library allows you to easily log from any file in your project.

## How to use:
### Main file:
`Logger.start(logFileName, level, mainClassOfProgram);`

Params:
- `logFileName` is the file.
  - E.g. `log.md` or `my-logger.txt`
- `level` is the verbose of
    [java.util.logging Level](https://docs.oracle.com/javase/8/docs/api/java/util/logging/Level.html).
  - _Note: The level can be changed later on in your code with `Logger.LOGGER.setLevel(<level>)`,
     although it is unadvised to do so._
- `mainClassOfProgram` Any class in your project. This removes the package path from the log formatting.
  - E.g. `Main.class`

### Any other file you want to log from:
Simply use `Logger.LOGGER.<methods_from_java.util.logging>`
(see [java.util.logging Logger](https://docs.oracle.com/javase/8/docs/api/java/util/logging/Logger.html)).

E.g. 

- `Logger.LOGGER.severe("Test info logging: Severe");`
- `Logger.LOGGER.log(Level.FINE, "");`

Note: The level must be set to something lower or equal to `FINE` if you want to log the second line.
You can also set the level to `Level.ALL`.
